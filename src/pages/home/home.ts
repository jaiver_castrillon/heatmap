import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  map: mapboxgl.Map;

  constructor(public navCtrl: NavController) {
    //Set API KEY de Mapbox
    mapboxgl.accessToken = 'pk.eyJ1IjoiamFpdmVyIiwiYSI6ImNqeWpmazJoMzAzN2EzaXBraDA0N2czaWkifQ.4yHAJCTWDcRzdbiMXGEzxA';
  }

  //Initialize map 
  ionViewDidLoad() {
    this.map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/dark-v10',
      center: [-75.025634765625,
      5.665184914439158],
      zoom: 2
    });
  }

  onMapLoad(){
		
		this.map.addSource('earthquakes', {
	        "type": "geojson",
	        "data": "https://docs.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson"
	    });
      

	/*
	this.map.addSource('earthquakes', {
		"type": "geojson",
		"data": {
			"type": "FeatureCollection",
			"features": [
				{
				"type": "Feature",
				"properties": 
					{"mag": 40.3}
				,
				"geometry": {
					"type": "Point",
					"coordinates": [
					-74,
					5.067
					]
				}
				},
				{
				"type": "Feature",
				"properties": {"mag": 1},
				"geometry": {
					"type": "Point",
					"coordinates": [
					-75.6298828125,
					6.189707330332202
					]
				}
				}
			]
			}
		});*/


	this.map.addLayer({
		"id": "earthquakes-heat",
		"type": "heatmap",
		"source": "earthquakes",
		"maxzoom": 9,
		"paint": {
			// Increase the heatmap weight based on frequency and property magnitude
			"heatmap-weight": [
				"interpolate",
				["linear"],
				["get", "mag"],
				0, 0,
				6, 1
			],
			// Increase the heatmap color weight weight by zoom level
			// heatmap-intensity is a multiplier on top of heatmap-weight
			"heatmap-intensity": [
				"interpolate",
				["linear"],
				["zoom"],
				0, 1,
				9, 3
			],
			// Color ramp for heatmap.  Domain is 0 (low) to 1 (high).
			// Begin color ramp at 0-stop with a 0-transparancy color
			// to create a blur-like effect.
			"heatmap-color": [
				"interpolate",
				["linear"],
				["heatmap-density"],
				0, "rgba(33,102,172,0)",
				0.2, "rgb(103,169,207)",
				0.4, "rgb(209,229,240)",
				0.6, "rgb(253,219,199)",
				0.8, "rgb(239,138,98)",
				1, "rgb(178,24,43)"
			],
			// Adjust the heatmap radius by zoom level
			"heatmap-radius": [
				"interpolate",
				["linear"],
				["zoom"],
				0, 2,
				9, 20
			],
			// Transition from heatmap to circle layer by zoom level
			"heatmap-opacity": [
				"interpolate",
				["linear"],
				["zoom"],
				7, 1,
				9, 0
			],
		}
	}, 'waterway-label');
  
  }


}
